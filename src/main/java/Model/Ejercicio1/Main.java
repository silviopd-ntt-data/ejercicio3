
package Model.Ejercicio1;


import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int valor1 = 2;
        int valor2 = 2;
        String operacion = "/";

        Calculadora cal = new Calculadora();
        double resultado = cal.calculadora2(valor1, valor2, operacion);

        if (resultado != Double.NEGATIVE_INFINITY && resultado != Double.POSITIVE_INFINITY) {
            System.out.println("El resultado es: "+resultado);
        }


    }

}
